﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace task9
{
	class Program
	{
		static void Main(string[] args)
		{
			ChaosArray<int> chaosArrayInt = new ChaosArray<int>();
			ChaosArray<string> chaosArrayString = new ChaosArray<string>();

			
			//Adding to chaosArray
			for(int i = 0; i < 15; i++)
			{
				try
				{
					chaosArrayInt.Insert(i); //inserting numbers
					chaosArrayString.Insert("Generics"); //inserting "generics"
				}catch(CustomException e)
				{
					Console.WriteLine(e.Message);
				}
				
			}

			// fetching from chaosArray
			for(int i = 0; i < 30; i++)
			{
				try
				{
					Console.WriteLine(chaosArrayInt.retrieveItem());
					Console.WriteLine(chaosArrayString.retrieveItem());
				}catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
				
			}

		}


		
		

	}
}
