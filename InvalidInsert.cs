﻿using System;

namespace task9
{
	public partial class CustomException : Exception
	{
		public CustomException(string message)
			: base(message)
		{

		}

	}
}
