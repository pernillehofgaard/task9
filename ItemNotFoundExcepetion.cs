﻿namespace task9
{
	public partial class CustomException
	{
		public class ItemNotFoundExcepetion : CustomException
		{
			public ItemNotFoundExcepetion(string message) : base(message)
			{
			}

			public ItemNotFoundExcepetion() : base("Couldn't find item")
			{
			}

		}

	}
}
