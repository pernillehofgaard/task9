﻿using System;
using static task9.CustomException;

namespace task9
{
	class ChaosArray <T> 
	{
		private Random random = new Random();
		private T[] chaosArray = new T[15];

		public T retrieveItem()
		{
			int i = random.Next(chaosArray.Length);

			if (chaosArray[i] == null || chaosArray[i].Equals(default(T)))
			{
				throw new ItemNotFoundExcepetion();
			}

			return chaosArray[i];
		}

		public void Insert(T item)
		{
			int i = random.Next(chaosArray.Length);
			if (chaosArray[i] == null || chaosArray[i].Equals(default(T)))
			{
				chaosArray[i] = item;
			}
			else
			{
				throw new InvalidInsertException();
			}
		}
	}
}
