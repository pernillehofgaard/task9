﻿namespace task9
{
	public partial class CustomException
	{
		public class InvalidInsertException : CustomException
		{
			public InvalidInsertException(string message) 
				: base(message) 
			{ 
			}

			public InvalidInsertException() 
				: base("Unavailable space exeption")
			{
			}
		}

	}
}
